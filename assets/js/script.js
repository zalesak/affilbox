$(function() {
    $('.menu-toggler').click(function(){
        $('.navbar-toggler').toggleClass('open');
        $('.mobilemenu').toggleClass('open'); 
    });

    $('.categories-toggler').click(function(){
        $('.categories__nav').toggleClass('open');
    });
});